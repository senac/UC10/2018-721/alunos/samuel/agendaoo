
import java.io.IOException;
import javax.swing.JOptionPane;


public class app {
  public static void main(String[] args) {

        Agenda agenda = new Agenda();
        String nome;
        String telefone;
        Pessoa pessoa;
        int opcao;
        String aux;

        for (;;) {
            String menu = String.format("########### Agenda de Contatos ###########\n"
                    + "                                Contatos(%d)\n"
                    + "1 - Novo\n"
                    + "2 - Deletar\n"
                    + "3 - Pesquiar\n"
                    + "4 - Lista Todos\n"
                    + "5 - Gravar agenda em disc0\n"
                    + "0 - Sair", agenda.getQuantidadeContatos());

            try {
                aux = JOptionPane.showInputDialog(menu);
                opcao = Integer.parseInt(aux);

                switch (opcao) {
                    case 0:
                        agenda.exportar();
                        System.exit(0);
                        break;
                    case 1:
                        nome = JOptionPane.showInputDialog("Digite nome:");
                        telefone = JOptionPane.showInputDialog("Digite Telefone:");
                        pessoa = new Pessoa(nome, telefone);
                        agenda.adicionarContato(pessoa);
                        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
                        break;
                    case 2:
                        if (!agenda.isVazia()) {
                            nome = JOptionPane.showInputDialog("Digite nome:");
                            agenda.removerContato(nome);
                            JOptionPane.showMessageDialog(null, "Deletado com sucesso!");
                        } else {
                            JOptionPane.showMessageDialog(null, "Agenda Vazia!");
                        }

                        break;

                    case 3:

                        if (!agenda.isVazia()) {
                            nome = JOptionPane.showInputDialog("Digite nome:");
                            pessoa = agenda.buscarContato(nome);
                            if (pessoa != null) {
                                JOptionPane.showMessageDialog(null, pessoa);
                            } else {
                                JOptionPane.showMessageDialog(null, "Contato nao encontrado!");
                            }

                        } else {
                            JOptionPane.showMessageDialog(null, "Agenda Vazia!");
                        }

                        break;

                    case 4:

                        if (!agenda.isVazia()) {

                            JOptionPane.showMessageDialog(null, agenda.getListaContatosParaImpressao());

                        } else {
                            JOptionPane.showMessageDialog(null, "Agenda Vazia!");
                        }
                        break;

                    case 5:

                       agenda.exportar();
                       JOptionPane.showMessageDialog(null, "Agenda Salva com sucesso!");

                        break;

                    default:
                        JOptionPane.showMessageDialog(null, "Opção Inválida !!!!");

                }

            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(null, "Falha ao converter valor... "
                        + "\nverifique a entrada", "Erro", JOptionPane.ERROR_MESSAGE);
            } catch (RuntimeException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Nao foi possivel gravar o arquivo!", "Erro", JOptionPane.ERROR_MESSAGE);
            }

        }

    }  
}
