
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Agenda {
    private String nome;
    
    private List<Pessoa> contatos;

    public Agenda() {
        this.contatos = new ArrayList<>();
    }

    public Agenda(String nome) {
        this();
        this.nome = nome;
    }

    

    public void adicionarContato(Pessoa p) {

        if (!this.isContatoNaAgenda(p)) {
            this.contatos.add(p);
        } else {
            throw new RuntimeException("Contato ja existente !");
        }

    }

    public void removerContato(Pessoa p) {
        if (this.isContatoNaAgenda(p)) {
            this.contatos.remove(p);
        } else {
            throw new RuntimeException("Contato não esta na agenda !");
        }
    }
    
         public int getQuantidadeContatos() {
        return this.contatos.size();
    }

    public Pessoa buscarContato(String nome) {
        for (int i = 0; i < this.getQuantidadeContatos(); i++) {
            if (this.contatos.get(i).getNome().equals(nome)) {
                return this.contatos.get(i);
            }
        }

        return null;
    }

    public String getListaContatosParaImpressao() {

        String listaContatos = "";

        for (Pessoa contato : contatos) {
            listaContatos += contato.toString() + "\n";
        }

        return listaContatos;

    }

    public boolean isContatoNaAgenda(Pessoa p) {
        return this.contatos.contains(p);
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    

    public boolean isVazia() {
        return this.contatos.isEmpty();
    }

    public void exportar() throws IOException {
        File file = new File("agenda.txt");

        if (!file.exists()) {
            file.createNewFile();
        }

        FileWriter fw = new FileWriter(file);

        for (Pessoa contato : contatos) {

            fw.write(contato.getNome() + "," + contato.getTelefone() + "\n");
        }

        fw.close();
    }

    void removerContato(String jose_da_Couve) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
