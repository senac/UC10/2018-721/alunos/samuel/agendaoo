
public class Pessoa {
    private String nome;
    private String telefone;

    public Pessoa(String nome, String telefone) {
        this.nome = nome;
        this.telefone = telefone;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return this.nome;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getTelefone() {
        return this.telefone;
    }

    @Override
    public String toString() {
        //Nome: Jose da Silva - Telefone:(27)9999-9999
        return "Nome: " + this.nome + " - " + "Telefone: " + this.telefone;
    }

    @Override
    public boolean equals(Object outraPessoa) {

        if (outraPessoa instanceof Pessoa) {
            //Temos certeza que é pessoa 
            // converter para referencia correta (Casting)
            Pessoa x = (Pessoa) outraPessoa;

            return this.nome.equals(x.getNome()) && this.telefone.equals(x.getTelefone());

        }

        return false;
    }    
}
