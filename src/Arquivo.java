
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Arquivo {
     public static void main(String[] args) {

      Pessoa p = new Pessoa("katrina", "(28) 8888-8888");
        Pessoa p2 = new Pessoa("Antonios", "(29) 9999-9999");
        Pessoa p3 = new Pessoa("Rafa Moreira mano", "(27) 7777-7777");
        Pessoa p4 = new Pessoa("Dellei", "(24) 4444-4444");
        Pessoa p5 = new Pessoa("Quemm te Perguntou", "(25) 5959-5555");

         List<Pessoa> listaDePessoas = new ArrayList<>();
        listaDePessoas.add(p);
        listaDePessoas.add(p2);
        listaDePessoas.add(p3);
        listaDePessoas.add(p4);
        listaDePessoas.add(p5);
        
        File file = new File("contatos.txt");

        try {
            FileWriter fw;
            if (!file.exists()) {
                file.createNewFile();
                fw = new FileWriter(file);
            } else {
                fw = new FileWriter(file, true);
            }

            for (Pessoa x : listaDePessoas) {
                fw.write("Nome:" + x.getNome() + " Telefone:" + x.getTelefone() + "\n");
            }

            fw.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
