
import java.util.ArrayList;
import java.util.List;


public class PessoaTeste {
    public static void main(String[] args) {

       Pessoa p = new Pessoa("katrina", "(28) 8888-8888");
        Pessoa p2 = new Pessoa("Antonios", "(29) 9999-9999");
        Pessoa p3 = new Pessoa("Rafa Moreira mano", "(27) 7777-7777");
        Pessoa p4 = new Pessoa("Dellei", "(24) 4444-4444");
        Pessoa p5 = new Pessoa("Quemm te Perguntou", "(25) 5959-5555");

        List<Pessoa> listaDePessoas = new ArrayList<>();
        listaDePessoas.add(p);
        listaDePessoas.add(p2);
        listaDePessoas.add(p3);
        listaDePessoas.add(p4);
        listaDePessoas.add(p5);
    

        System.out.println(listaDePessoas.size());
        System.out.println(p2);

        for (Pessoa x : listaDePessoas) {
            System.out.println(x);
        }

        Pessoa pX = new Pessoa("Jose da Couves", "(27) 9999-9999");
        Pessoa pY = new Pessoa("silas", "(27) 88888-8888");

        System.out.println("Jose esta na lista ?");
        System.out.println(listaDePessoas.contains(pX));

        System.out.println("Silas esta na lista ?");
        System.out.println(listaDePessoas.contains(pY));

        
        listaDePessoas.remove(pX);
        
        System.out.println("Jose esta na lista ?");
        System.out.println(listaDePessoas.contains(pX));


        /*
        for (int i = 0; i < listaDePessoas.size(); i++) {
            System.out.println(listaDePessoas.get(i));
        }*/
    }
}
