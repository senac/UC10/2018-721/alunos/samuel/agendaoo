
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class TestAgenda {
  public static void main(String[] args) throws IOException {

        Pessoa p = new Pessoa("katrina", "(28) 8888-8888");
        Pessoa p2 = new Pessoa("Antonios", "(29) 9999-9999");
        Pessoa p3 = new Pessoa("Rafa Moreira mano", "(27) 7777-7777");
        Pessoa p4 = new Pessoa("Dellei", "(24) 4444-4444");
        Pessoa p5 = new Pessoa("Quemm te Perguntou", "(25) 5959-5555");
    

        Agenda agenda = new Agenda();
        agenda.adicionarContato(p);
        agenda.adicionarContato(p2);
        agenda.adicionarContato(p3);
        agenda.adicionarContato(p4);
        agenda.adicionarContato(p5);
     
        System.out.println("Contatos:" + agenda.getQuantidadeContatos());
        agenda.removerContato(p3);
        agenda.removerContato(p5);
       
        System.out.println("Contatos:" + agenda.getQuantidadeContatos());
        agenda.removerContato("Quemm te Perguntou");
        System.out.println("Contatos:" + agenda.getQuantidadeContatos());

        System.out.println("Quemm te Perguntou ? " + agenda.isContatoNaAgenda(p5));
        System.out.println("Katrina? " + agenda.isContatoNaAgenda(p));

        File file = new File("teste.txt");

        if (!file.exists()) {
            file.createNewFile();

        }

        try {

            FileWriter fw = new FileWriter(file, true); //the true will append the new data
            fw.write("add a line\n");//appends the string to the file
            fw.close();
        } catch (IOException ioe) {
            System.err.println("IOException: " + ioe.getMessage());
        }

    }  
}
